from matplotlib import colors
import scipy.integrate as sp
import rosbag
import numpy as np
from tf.transformations import euler_from_quaternion
import matplotlib.pyplot as plt
import math
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse
from scipy import fftpack
yaw = []
time = []
roll = []
pitch = []
accel_x = []
accel_y = []
accel_z = []
ang_z = []
mag_x = []
mag_y = []
mag_z = []
utm_north = []
utm_east = []
t_g = []


bag2 = rosbag.Bag('/home/sahil/Downloads/gps_data.bag')
for topic,gps,t in bag2.read_messages(topics=['/gps_data']):
    utm_north.append(gps.utm_northing)
    utm_east.append(gps.utm_easting)
    t_g.append(gps.header.stamp.secs)


utm_n = np.array(utm_north)
utm_e = np.array(utm_east)
Time_gps = np.array(t_g)


bag = rosbag.Bag('/home/sahil/catkin_ws/src/Analysis/trim_imu.bag')
for topic,Imu,t in bag.read_messages(topics=['/imu_data']):
    accel_x.append(Imu.linear_acceleration.x)
    accel_y.append(Imu.linear_acceleration.y)
    accel_z.append(Imu.linear_acceleration.z)
    xyz = [Imu.orientation.x, Imu.orientation.y, Imu.orientation.z, Imu.orientation.w]
    ang_z.append(Imu.angular_velocity.z)
    rpy = euler_from_quaternion(xyz)
    roll.append(rpy[0])
    pitch.append(rpy[1])
    yaw.append(rpy[2])
    time.append(Imu.header.stamp.secs)

Time = np.array(time)
accelx = np.array(accel_x)
accely = np.array(accel_y)
ang_rate = np.array(ang_z)
yaw_rate = np.array(yaw)
for topic,MagneticField,t in bag.read_messages(topics=['/mag_data']):
    mag_x.append(MagneticField.magnetic_field.x)
    mag_y.append(MagneticField.magnetic_field.y)
    mag_z.append(MagneticField.magnetic_field.z)

plt.figure('GPS Data')
plt.plot(utm_e[100:], utm_n[100:], '.')
#185-344
#7400 - 13760
#0-30----0 - 1200
#256-348---10240 - 13920
#378-436---15120 - 17440
#473-487---18920 - 19480
#567-615---22680 - 24600---568-614
#649-680---25960 - 27200---654

#705-775---28129 - 31120
#865-872---34600 - 34880
#904-927---36160 - 37080 
#1036 - 1062 bias 41440
#1131-1152
magx = np.array(mag_x)
magy = np.array(mag_y)
magz = np.array(mag_z)

vel_x= np.diff(utm_n)
vel_y= np.diff(utm_e)
time_d = np.diff(Time_gps)
velo_x = vel_x/time_d
velo_y = vel_y/time_d
prop_time = Time - 1614795297
#----- Acceleration bias correction--------
accelx[0:7399] = accelx[0:7399] - np.mean(accelx[0:1200])
accelx[7400:15120] = accelx[7400:15120]- np.mean(accelx[10240:13920])
accelx[15121:18919] = accelx[15121:18919] - np.mean(accelx[15240:17440])
accelx[18920:22719] = accelx[18920:22719] - np.mean(accelx[18920:19480])
accelx[22720:26119] = accelx[22720:26119] - np.mean(accelx[22720:24560])
accelx[26120:28200] = accelx[26120:28200] - np.mean(accelx[26120:27200])
accelx[28201:34599] = accelx[28201:34599] - np.mean(accelx[28201:31120])
accelx[34600:36159] = accelx[34600:36159] - np.mean(accelx[34600:34880])
accelx[36160:40159] = accelx[36160:40159] - np.mean(accelx[36160:37080])
accelx[40160:41599] = accelx[40160:41599] - np.mean(accelx[40160:40240])
accelx[41600:45199] = accelx[41600:45199] - np.mean(accelx[41600:42480])
accelx[45200:] = accelx[45200:] - np.mean(accelx[45200:46000])

velo_new = sp.cumtrapz(accelx, x=Time, initial=0)
plt.figure()
plt.title('Integrated velocity comparison')
plt.xlabel('Time(s)')
plt.ylabel('Magnitude')
plt.plot(Time - Time[0], velo_new, 'r')
plt.plot(Time - Time[0], accelx, 'g')
plt.plot(Time_gps[1:] - Time_gps[1], np.sqrt(np.square(velo_x) + np.square(velo_y)), 'b')
plt.legend(('Velocity', 'Acceleration', 'GPS velocity'))
# Hard Iron and Soft iron bias
hi_x = (0.129 + 0.274)/2.0
hi_y = (0.097 + 0.1845)/2.0

#Corrected data
mag_correct_x = magx - hi_x
mag_correct_x = mag_correct_x/1.75
mag_correct_y = magy - hi_y
yaw_mag = np.arctan2(mag_correct_y, mag_correct_x)
yaw_angle = sp.cumtrapz(ang_rate, x=Time, initial =0)
yaw_angle2 = sp.cumtrapz(yaw_rate, x=Time, initial=0)

fmx = fftpack.fft(yaw_mag)
fmxp = fftpack.fftfreq(yaw_mag.shape[-1], d=0.025)

fgx = fftpack.fft(yaw_angle)
fgxp = fftpack.fftfreq(yaw_angle.shape[-1], d=0.025)



for i in range(fmxp.size):
    if(np.absolute(fmxp[i]) > 1.0):
        fmx[i] = 0

for i in range(fgxp.size):
    if(np.absolute(fgxp[i]) < 0.0005):
        fgx[i] = 0

yaw_mf = fftpack.ifft(fmx)
yaw_gf = fftpack.ifft(fgx)
alpha = 0.50
yaw_compli = ((1-alpha)*(yaw_mf)) + ((alpha)*(yaw_gf))
velocity_acc = sp.cumtrapz(accelx, x=Time, initial=0)
plt.plot(Time_gps[1:]-65699,np.sqrt(np.square(velo_x) + np.square(velo_y)))
plt.plot(prop_time, accelx)
plt.plot(prop_time, velocity_acc)
#-----------Dead Reckoning-----------#
y_accel = ang_rate * velocity_acc
plt.figure('Acceleration from Angular z and Vel.x')
plt.plot(Time, y_accel, 'blue')
plt.plot(Time, (accely + 1.0), 'red')

vn = velocity_acc * np.sin(yaw_angle)
ve = velocity_acc * np.cos(yaw_angle)
print(ve.size)
print(Time.size)
xn = sp.cumtrapz(vn, x = Time, initial = 0)
xe = sp.cumtrapz(ve, x = Time, initial = 0)
plt.figure('Dead Reckoning')
plt.plot(xe, xn)
plt.show()