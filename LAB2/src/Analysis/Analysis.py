#!/usr/bin/python

import matplotlib.pyplot as plt
import csv
from rospy import topics
from tf.transformations import euler_from_quaternion
import numpy
import rosbag

#Defining arrays
accel_x = []
accel_y = []
accel_z = []
mag_x = []
mag_y = []
mag_z = []
time = []
roll = []
pitch = []
yaw = []
rpy = []
ang_x = []
ang_y = []
ang_z = []
#rpy = euler_from_quaternion(q0, q1, q2, q3)
bag = rosbag.Bag('/home/sahil/catkin_ws/src/Analysis/Final_data.bag')
for topic,Imu,t in bag.read_messages(topics=['/IMU']):
    accel_x.append(Imu.linear_acceleration.x)
    accel_y.append(Imu.linear_acceleration.y)
    accel_z.append(Imu.linear_acceleration.z)
    ang_x.append(Imu.angular_velocity.x)
    ang_y.append(Imu.angular_velocity.y)
    ang_z.append(Imu.angular_velocity.z)
    xyz = [Imu.orientation.x, Imu.orientation.y, Imu.orientation.z, Imu.orientation.w]
    rpy = euler_from_quaternion(xyz)
    roll.append(rpy[0])
    pitch.append(rpy[1])
    yaw.append(rpy[2])
    time.append(Imu.header.stamp.secs)

for topic,MagneticField,t in bag.read_messages(topics=['/MAG']):
    mag_x.append(MagneticField.magnetic_field.x)
    mag_y.append(MagneticField.magnetic_field.y)
    mag_z.append(MagneticField.magnetic_field.z)

accelx = numpy.array(accel_x)
accely = numpy.array(accel_y)
accelz = numpy.array(accel_z)
mn = numpy.mean(accelz)
sd = numpy.std(accelx)
Time = numpy.array(time)
magx = numpy.array(mag_x)
magy = numpy.array(mag_y)
magz = numpy.array(mag_z)
r1 = numpy.array(roll)
p1 = numpy.array(pitch)
y1 = numpy.array(yaw)
anx = numpy.array(ang_x)
any = numpy.array(ang_y)
anz = numpy.array(ang_z)
print(numpy.mean(anx))
print(numpy.mean(any))
print(numpy.mean(anz))
print(numpy.mean(accelx))
print(numpy.mean(accely))
print(numpy.mean(accelz))
plt.rc('font', size=8)
plt.figure(0)
plt.subplot(231)
plt.title('Acceleration-x vs Time')
plt.xlabel('Time')
plt.ylabel('Accel_x')
plt.plot(Time, accelx, '.')

plt.subplot(232)
plt.title('Acceleration-y vs Time')
plt.plot(Time, accely, '.')
plt.xlabel('Time')
plt.ylabel('Accel_y')

plt.subplot(233)
plt.title('Acceleration-z vs Time')
plt.plot(Time, accelz, '.')
plt.xlabel('Time')
plt.ylabel('Accel_z')

plt.subplot(234)
plt.title('Error Distribution-1')
plt.xlabel('Accel_x')
plt.hist(accelx-numpy.mean(accelx), color='red', edgecolor='black', bins=30)

plt.subplot(235)
plt.title('Error Distribution-2')
plt.xlabel('Accel_y')
plt.hist(accely-numpy.mean(accely), color='red', edgecolor='black', bins=30)

plt.subplot(236)
plt.title('Error Distribution for Accel-z ')
plt.xlabel('Accel_z')
plt.hist(accelz-numpy.mean(accelz), color='red', edgecolor='black', bins=30)


plt.figure(1)
plt.subplot(231)
plt.title('Magnetic-x vs Time')
plt.xlabel('Time')
plt.ylabel('Mag_x')
plt.plot(Time, magx, '.')

plt.subplot(232)
plt.title('Magnetic-y vs Time')
plt.plot(Time, magy, '.')
plt.xlabel('Time')
plt.ylabel('Mag_y')

plt.subplot(233)
plt.title('Magnetic-z vs Time')
plt.plot(Time, magz, '.')
plt.xlabel('Time')
plt.ylabel('Mag_z')

plt.subplot(234)
plt.title('Error Distribution-1')
plt.xlabel('Mag_x')
plt.hist(magx-numpy.mean(magx), color='red', edgecolor='black', bins=30)

plt.subplot(235)
plt.title('Error Distribution-2')
plt.xlabel('Mag_y')
plt.hist(magy-numpy.mean(magy), color='red', edgecolor='black', bins=30)

plt.subplot(236)
plt.title('Error Distribution-3')
plt.xlabel('Mag_z')
plt.hist(magz-numpy.mean(magz), color='red', edgecolor='black', bins=30)


plt.figure(2)
plt.subplot(221)
plt.plot(Time, r1, '.')
plt.title('Roll vs Time')

plt.subplot(222)
plt.plot(Time, p1, '.')
plt.title('Pitch vs Time')

plt.subplot(223)
plt.plot(Time,y1,'.')
plt.title('Yaw vs Time')

plt.figure(3)
plt.subplot(231)
plt.title('Angular-x vs Time')
plt.xlabel('Time')
plt.ylabel('Ang_x')
plt.plot(Time, anx, '.')

plt.subplot(232)
plt.title('Angular-y vs Time')
plt.plot(Time, any, '.')
plt.xlabel('Time')
plt.ylabel('Ang_y')

plt.subplot(233)
plt.title('Angular-z vs Time')
plt.plot(Time, anz, '.')
plt.xlabel('Time')
plt.ylabel('Ang_z')

plt.subplot(234)
plt.title('Error Distribution-Ang_x')
plt.xlabel('Ang_x')
plt.hist(anx-numpy.mean(anx), color='red', edgecolor='black', bins=30)

plt.subplot(235)
plt.title('Error Distribution-Ang_y')
plt.xlabel('Ang_y')
plt.hist(any-numpy.mean(any), color='red', edgecolor='black', bins=30)

plt.subplot(236)
plt.title('Error Distribution for Ang-z')
plt.xlabel('Ang_z')
plt.hist(anz-numpy.mean(anz), color='red', edgecolor='black', bins=30)

plt.show()
