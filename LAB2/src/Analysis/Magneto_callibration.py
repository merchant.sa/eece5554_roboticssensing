#!/usr/bin/python

from matplotlib import colors
import scipy.integrate as sp
import rosbag
import numpy as np
from tf.transformations import euler_from_quaternion
import matplotlib.pyplot as plt
import math
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse
from scipy import fftpack
yaw = []
time = []
roll = []
pitch = []
accel_x = []
accel_y = []
accel_z = []
ang_z = []
mag_x = []
mag_y = []
mag_z = []
utm_north = []
utm_east = []
t_g = []

bag = rosbag.Bag('/home/sahil/catkin_ws/src/Analysis/circle.bag')
for topic,Imu,t in bag.read_messages(topics=['/imu_data']):
    accel_x.append(Imu.linear_acceleration.x)
    accel_y.append(Imu.linear_acceleration.y)
    accel_z.append(Imu.linear_acceleration.z)
    xyz = [Imu.orientation.x, Imu.orientation.y, Imu.orientation.z, Imu.orientation.w]
    ang_z.append(Imu.angular_velocity.z)
    rpy = euler_from_quaternion(xyz)
    roll.append(rpy[0])
    pitch.append(rpy[1])
    yaw.append(rpy[2])
    time.append(Imu.header.stamp.secs)

for topic,MagneticField,t in bag.read_messages(topics=['/mag_data']):
    mag_x.append(MagneticField.magnetic_field.x)
    mag_y.append(MagneticField.magnetic_field.y)
    mag_z.append(MagneticField.magnetic_field.z)

ang_rate = np.array(ang_z)
yaw_rate = np.array(yaw)
Time = np.array(time)
accelx = np.array(accel_x)
accely = np.array(accel_y)
accelz = np.array(accel_z)
#----Car was stationary for ~45 seconds----
magx = np.array(mag_x[1800:])
magy = np.array(mag_y[1800:])
magz = np.array(mag_z[1800:])
ang_rate = np.array(ang_z)
yaw_rate = np.array(yaw)
Time = np.array(time)

yaw_angle = sp.cumtrapz(ang_rate, x=Time, initial =0)
yaw_angle2 = sp.cumtrapz(yaw_rate, x=Time, initial=0)
plt.figure('Magx v Magy Before Correction')
plt.title('Magnetic data before correction')
plt.xlabel('Magnetic x (Gauss)')
plt.ylabel('Magnetic y (Gauss)')
plt.grid(axis='both')
plt.plot(magx, magy, '.')

#Hard iron correction
hard_iron_x = (np.min(magx) + np.max(magx))/2.0
hard_iron_y = (np.min(magy) + np.max(magy))/2.0

magx = magx - hard_iron_x
magy = magy - hard_iron_y

magx = magx/(1.75) #soft iron approximate
yaw_mag = np.arctan2(magy, magx)
plt.figure('Magx v Magy After Correction')
plt.title('Magnetic data before correction')
plt.xlabel('Magnetic x (Gauss)')
plt.ylabel('Magnetic y (Gauss)')
plt.grid(axis='both')
plt.plot(magx, magy, '.')
plt.show()