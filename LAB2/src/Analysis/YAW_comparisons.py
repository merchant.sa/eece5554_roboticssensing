#!/usr/bin/python

from matplotlib import colors
import scipy.integrate as sp
import rosbag
import numpy as np
from tf.transformations import euler_from_quaternion
import matplotlib.pyplot as plt
import math
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse
from scipy import fftpack
yaw = []
time = []
roll = []
pitch = []
accel_x = []
accel_y = []
accel_z = []
ang_z = []
mag_x = []
mag_y = []
mag_z = []
utm_north = []
utm_east = []
t_g = []


bag2 = rosbag.Bag('/home/sahil/Downloads/gps_data.bag')
for topic,gps,t in bag2.read_messages(topics=['/gps_data']):
    utm_north.append(gps.utm_northing)
    utm_east.append(gps.utm_easting)
    t_g.append(gps.header.stamp.secs)

utm_n = np.array(utm_north)
utm_e = np.array(utm_east)
Time_gps = np.array(t_g)


bag = rosbag.Bag('/home/sahil/catkin_ws/src/Analysis/trim_imu.bag')
for topic,Imu,t in bag.read_messages(topics=['/imu_data']):
    accel_x.append(Imu.linear_acceleration.x)
    accel_y.append(Imu.linear_acceleration.y)
    accel_z.append(Imu.linear_acceleration.z)
    xyz = [Imu.orientation.x, Imu.orientation.y, Imu.orientation.z, Imu.orientation.w]
    ang_z.append(Imu.angular_velocity.z)
    rpy = euler_from_quaternion(xyz)
    roll.append(rpy[0])
    pitch.append(rpy[1])
    yaw.append(rpy[2])
    time.append(Imu.header.stamp.secs)

for topic,MagneticField,t in bag.read_messages(topics=['/mag_data']):
    mag_x.append(MagneticField.magnetic_field.x)
    mag_y.append(MagneticField.magnetic_field.y)
    mag_z.append(MagneticField.magnetic_field.z)


magx = np.array(mag_x[4000:])
magy = np.array(mag_y[4000:])
magz = np.array(mag_z)

ang_rate = np.array(ang_z[4000:])
yaw_rate = np.array(yaw[4000:])
Time = np.array(time)
accelx = np.array(accel_x)
accely = np.array(accel_y)
accelz = np.array(accel_z)
yaw_angle = sp.cumtrapz(ang_rate, x=Time[4000:], initial =0)
yaw_angle2 = sp.cumtrapz(yaw_rate, x=Time[4000:], initial=0)
vel_x = sp.cumtrapz(accelx, x=Time, initial =0)
vel_y = sp.cumtrapz(accely, x=Time, initial =0)
vel_z = sp.cumtrapz(accelz, x=Time, initial =0)



#---------Part 2b-1---------------
#plt.figure('Yaw Integrated from Angular')
#plt.plot(Time, yaw_angle, '.')
#plt.figure('Angular raw')
#plt.plot(Time, ang_rate, '.')
#plt.figure('Yaw orientation')
#plt.plot(Time, yaw_angle2, '.')
plt.figure('Magx v Magy')
plt.grid(axis='both')
plt.plot(magx, magy, '.')

# Hard Iron and Soft iron bias
hi_x = (0.129 + 0.274)/2.0
hi_y = (0.097 + 0.1845)/2.0

#Corrected data
mag_correct_x = magx - hi_x
mag_correct_x = mag_correct_x/1.75
mag_correct_y = magy - hi_y
yaw_mag = np.arctan2(mag_correct_y, mag_correct_x)
plt.figure("YAW")
plt.xlabel('Time(s)')
plt.ylabel('Yaw Angle')
plt.plot(Time[4000:], (yaw_mag*180/math.pi), 'blue')
plt.plot(Time[4000:], (yaw_angle*180/math.pi),'red')
plt.legend(('Yaw Magnetic', 'Yaw Gyro'))

fmx = fftpack.fft(yaw_mag)
fmxp = fftpack.fftfreq(yaw_mag.shape[-1], d=0.025)

fgx = fftpack.fft(yaw_angle)
fgxp = fftpack.fftfreq(yaw_angle.shape[-1], d=0.025)


for i in range(fmxp.size):
    if(np.absolute(fmxp[i]) > 1.0):
        fmx[i] = 0

for i in range(fgxp.size):
    if(np.absolute(fgxp[i]) < 0.0005):
        fgx[i] = 0

yaw_mf = fftpack.ifft(fmx)
yaw_gf = fftpack.ifft(fgx)
alpha = 0.50
yaw_compli = ((1-alpha)*(yaw_mf)) + ((alpha)*(yaw_gf))

plt.figure('Filtered comparison')
plt.title('IMU Yaw vs Filtered Yaw')
plt.xlabel('Time(s)')
plt.ylabel('Yaw angle(Degrees)')
plt.plot(Time[4000:], yaw_compli*180/math.pi, 'black')
plt.plot(Time[4000:], yaw_angle*180/math.pi, 'blue')
plt.legend(('Complimentary yaw', 'IMU Yaw'))
plt.show()