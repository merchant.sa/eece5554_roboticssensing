#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import utm
import serial
from std_msgs.msg import Header
from Lab1.msg import custom_gps

if __name__ == '__main__':
    rospy.init_node('GPS_Driver')
    port = serial.Serial('/dev/ttyUSB0', 4800, timeout = 3.)
    rospy.logdebug("Starting Sensor")
    gps_pub = rospy.Publisher('gps', custom_gps, queue_size=5)

    try:
        while not rospy.is_shutdown():
            line = port.readline()
            extract = line.split(',')
            data = custom_gps()
            print(line)
            if extract[0] == '':
                rospy.logwarn('No data')

            elif extract[0].strip() == '$GPGGA':
                data.rawdata = line
                data.header = Header()
                data.header.stamp = rospy.Time.now()
                lat_raw = extract[2]
                latitude = float(lat_raw)/100.0
                if(extract[3] == 'S'):
                    latitude = -latitude
                data.Latitude = str(latitude) + str(extract[3])
                
                lon_raw = extract[4]
                longitude = float(lon_raw)/100.0
                if(extract[5] == 'W'):
                    longitude = -longitude
                data.Longitude = str(longitude) + str(extract[5])
                
                altitude = float(extract[8])
                data.altitude = altitude
                utm_convert = utm.from_latlon(latitude, longitude)
                data.utm_northing = utm_convert[0]
                data.utm_easting = utm_convert[1]
                
                utm_zone = str(utm_convert[2]) + str(utm_convert[3])
                data.utm_zone = utm_zone
                gps_pub.publish(data)
    
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo('Shutting down GPS')
