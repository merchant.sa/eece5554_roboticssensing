#!/usr/bin/python

import matplotlib.pyplot as plt
import csv
import numpy
import rosbag

#Defining arrays
utm_north = []
utm_east = []
time = []

bag = rosbag.Bag('/home/suriyaelango/sahil_bagfile/Moving_data.bag')
for topic,custom_gps,t in bag.read_messages(topics=['gps']):
    utm_north.append(custom_gps.utm_northing)
    utm_east.append(custom_gps.utm_easting)
    time.append(custom_gps.header.stamp.secs)


utm_n = numpy.array(utm_north)
utm_e = numpy.array(utm_east)
mn = numpy.mean(utm_e)
Time = numpy.array(time)


plt.subplot(221)
plt.title('Northing vs Time')
plt.xlabel('Time')
plt.ylabel('UTM_northing')
plt.plot(Time, utm_n, '.')

plt.subplot(222)
plt.title('Easting vs Time')
plt.plot(Time, utm_e, '.')
plt.xlabel('Time')
plt.ylabel('UTM_Easting')

plt.subplot(223)
m,c = numpy.polyfit(utm_e, utm_n, 1)
plt.plot(utm_e,utm_n, '.')
plt.plot(utm_e, m*utm_e + c)
plt.title('Movement Data')
plt.xlabel('UTM_easting')
plt.ylabel('UTM_northing')

plt.show()