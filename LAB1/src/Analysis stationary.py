#!/usr/bin/python

import matplotlib.pyplot as plt
import csv
import numpy
import rosbag

utm_north = []
utm_east = []
time = []


bag = rosbag.Bag('/home/suriyaelango/sahil_bagfile/stationary_data.bag')
for topic,custom_gps,t in bag.read_messages(topics=['gps']):
    utm_north.append(custom_gps.utm_northing)
    utm_east.append(custom_gps.utm_easting)
    time.append(custom_gps.header.stamp.secs)


utm_n = numpy.array(utm_north)
utm_e = numpy.array(utm_east)
me = numpy.mean(utm_e)
mn = numpy.mean(utm_n)
Time = numpy.array(time)

plt.subplot(121)
plt.title('Northing vs Time with mean(Stationary)')
plt.xlabel('Time(s)')
plt.ylabel('UTM_northing(m)')
plt.scatter(Time, utm_n)
plt.hlines(mn, numpy.min(Time), numpy.max(Time), colors = 'red')

plt.subplot(122)
plt.title('Easting vs Time with mean(Stationary)')
plt.xlabel('Time(s)')
plt.ylabel('UTM_Easting(m)')
plt.scatter(Time, utm_e)
plt.hlines(me, numpy.min(Time), numpy.max(Time), colors='red')

plt.show()